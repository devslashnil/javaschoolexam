package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    private int getRows(List<Integer> inputNumbers) {
        double rows = (-1 + Math.sqrt(1 + 4 * 2 * inputNumbers.size())) / 2;
        if (rows % 1 != 0) {
            throw new CannotBuildPyramidException();
        }
        return (int) rows;
    }

    private void sortNumbers(List<Integer> inputNumbers) {
        if (inputNumbers.stream().anyMatch(Objects::isNull)) {
            throw new CannotBuildPyramidException();
        }
        Collections.sort(inputNumbers);
    }

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int rows = getRows(inputNumbers);
        sortNumbers(inputNumbers);
        Iterator<Integer> iter = inputNumbers.iterator();
        int[][] pyr = new int[rows][rows * 2  - 1];
        for (int i = 0; i < rows; i++) {
            for (int j = rows - i - 1; j < rows + i; j += 2) {
                pyr[i][j] = iter.next();
            }
        }
        return pyr;
    }

}
