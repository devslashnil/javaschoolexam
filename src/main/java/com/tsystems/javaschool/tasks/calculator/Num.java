package com.tsystems.javaschool.tasks.calculator;

public class Num extends Token {
    final double number;

    Num(String value) {
        super('n');
        this.number = Double.valueOf(value);
    }
}
