package com.tsystems.javaschool.tasks.calculator;

public class Token {
    private final char value;

    Token(char value) {
        this.value = value;
    }

    public boolean valueEqualsTo(char ...chars) {
        boolean res = false;
        for (char c : chars) {
            if (value == c) {
                res = true;
            }
        }
        return res;
    }
}
