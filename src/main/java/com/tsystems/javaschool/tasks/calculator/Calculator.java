package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

public class Calculator {
    private Iterator<Token> iter;
    private Token cur;
    private Token next;

    private void tokenize(String statement) {
        ArrayList<Token> tokens = new ArrayList<>();
        Token last = null;
        Token tokenToAdd = null;
        int openBrackets = 0;
        int closedBrackets = 0;
        for (int i = 0; i < statement.length(); i++) {
            char c = statement.charAt(i);
            if (c == '*' || c == '/' || c == '+' || c == '-') {
                if (last != null && last.valueEqualsTo('*', '/', '+', '-', '(')) {
                    throw new IllegalArgumentException("Not valid statement");
                }
                tokenToAdd = new Token(c);
                tokens.add(tokenToAdd);
            } else if (c == ')') {
                closedBrackets++;
                if (last != null && last.valueEqualsTo('(')) {
                    throw new IllegalArgumentException("Not valid statement");
                }
                tokenToAdd = new Token(c);
                tokens.add(tokenToAdd);
            }  else if (c == '(') {
                openBrackets++;
                if (last != null && last.valueEqualsTo(')')) {
                    throw new IllegalArgumentException("Not valid statement");
                }
                tokenToAdd = new Token(c);
                tokens.add(tokenToAdd);
            } else if (Character.isDigit(c) || c == '.') {
                int numEnd = i + 1;
                while (numEnd < statement.length() &&
                        (Character.isDigit(statement.charAt(numEnd)) || statement.charAt(numEnd) == '.')) {
                    numEnd++;
                }
                tokenToAdd = new Num(statement.substring(i, numEnd));
                tokens.add(tokenToAdd);
                i = numEnd - 1;
            } else if (!Character.isWhitespace(c)) {
                throw new IllegalArgumentException("Not valid statement");
            }
            last = tokenToAdd;
        }
        if (openBrackets != closedBrackets) {
            throw new IllegalArgumentException("Not valid statement");
        }
        iter = tokens.iterator();
    }

    private void iterate() {
        cur = next;
        next = iter.hasNext() ? iter.next() : null;
    }

    private double sum() {
        double res = product();
        while (next != null && next.valueEqualsTo('+', '-')) {
            iterate();
            if (cur.valueEqualsTo('+')) {
                res += product();
            } else {
                res -= product();
            }
        }
        return res;
    }

    private double product() {
        double res = sign();
        while (next != null && next.valueEqualsTo('*', '/')) {
            iterate();
            if (cur.valueEqualsTo('*')) {
                res *= sign();
            } else {
                res /= sign();
            }
        }
        return res;
    }

    private double sign() {
        double res;
        if (next != null && next.valueEqualsTo('-')) {
            iterate();
            res = -brackets();
        } else {
            res = brackets();
        }
        return res;
    }

    private double brackets() {
        double res;
        if (next != null && next.valueEqualsTo('n')) {
            iterate();
            return ((Num) cur).number;
        } else if (next != null && next.valueEqualsTo('(')) {
            iterate();
            res = sum();
            if (next != null && !next.valueEqualsTo(')')) {
                throw new IllegalArgumentException("Not valid statement");
            }
            iterate();
            return res;
        } else {
            throw new IllegalArgumentException("Not valid statement");
        }
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            if (statement == null) {
                return null;
            }
            tokenize(statement);
            iterate();
            DecimalFormat df = new DecimalFormat("#.####");
            df.setRoundingMode(RoundingMode.HALF_UP);
            double res = sum();
            if (Double.isInfinite(res)) {
                return  null;
            }
            return df.format(res);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

}
